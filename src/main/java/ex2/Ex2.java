package ex2;

import javax.swing.*;

public class Ex2 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Main");
        JTextField textField = new JTextField();
        JButton button = new JButton("Reverse");

        textField.setBounds(0, 10, 95, 30);
        button.setBounds(0, 40, 95, 30);

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        button.addActionListener(e -> {
            StringBuilder output = new StringBuilder(textField.getText()).reverse();
            System.out.println(output);
        });

        frame.add(textField);
        frame.add(button);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
